/* A generic stack setup
*  Taken from Jerry Cain's code.
*  Stanford Programming Paradigms series
*/

#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef struct {
    void *elems;
    int  elemSize;
    int  logLength;
    int  allocLength;
} stack;

static void StackGrow(stack *s) {
    s->allocLength *= 2;
    s->elems = realloc(s->elems, s->allocLength * s->elemSize);
}

void StackNew(stack *s, int elemSize) {
    assert(s->elemSize > 0);
    s->elemSize    = elemSize;
    s->logLength   = 0;
    s->allocLength = 4;
    s->elems       = malloc(4 * elemSize);
    assert(s->elems != NULL);
}

void StackDispose(stack *s) {
    free(s->elems);
}

void StackPush(stack *s, void *elemAddr) {
    if(s->logLength == s->allocLength)
        StackGrow(s);

    void *target = (char *)s->elems +
        s->logLength * s->elemSize;
    memcpy(target, elemAddr, s->elemSize);
    s->logLength++;
}

void StackPop(stack *s, void *elemAddr) {
    void *source = (char *) s->elems +
                   (s->logLength - 1) * s->elemSize;
    memcpy(elemAddr, source, s->elemSize);
    s->logLength--;
}
