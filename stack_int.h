/* Integer Stack code taken from Jerry Cain's
   Stanford Programming Paradigms series.

   Jan-Micheal Wells April, 12, 2016
*/

typedef struct {
    int  *elems;
    int  elemSize;
    int  loglength;
    int  alloclength;
}stack;

void StackNew    (stack *s);
void StackDispose (stack *s);
void StackPush   (stack *s, int value);
int StackPop     (stack *s);
