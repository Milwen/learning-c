/* Testing the stack lessons discussed by Jerry Cain
 * in the Standford Programming Paradigms Series.
 * I created 2 integer stacks and used fork to run
 * 2 seperate processes.
 *
 * Jan-Micheal Wells April 12, 2016
*/

#include <stdio.h>
#include <unistd.h>
#include "stack_int.h"

int main() {
    const int item1 = 5;
    const int item2 = 25;
    const int item3 = 14;
    const int item4 = 36;
    const int item5 = 7;
    const int item6 = 21;

    int   result = 0;
    stack intStack;
    stack intStack2;
    pid_t pid;

    pid = fork();
    if (pid == 0) {
        StackNew(&intStack2);
        StackPush(&intStack2, item4);
        StackPush(&intStack2, item5);
        StackPush(&intStack2, item6);

        result += StackPop(&intStack2);
        result += StackPop(&intStack2);
        result += StackPop(&intStack2);

        StackDispose(&intStack2);
        printf("result2: %i\n", result);
    }
    else {
        StackNew(&intStack);
        StackPush(&intStack, item1);
        StackPush(&intStack, item2);
        StackPush(&intStack, item3);

        result += StackPop(&intStack);
        result += StackPop(&intStack);
        result += StackPop(&intStack);

        StackDispose(&intStack);
        printf("result1: %i\n", result);
    }

    return 0;
}
