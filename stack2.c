/* Testing the generic stack lesson
 * taken from Jerry Cain's lectures
 * Stanford Programming Paradigms series
 * using fork to run seperate processes.
 *
 * Jan-Micheal Wells April 12, 2016
 */

#include <stdio.h>
#include <unistd.h>
#include "stack_gen.h"

int main() {
    int   balls[3]    = {1,2,3};
    float rates[3]    = {0.10, 1.45, 0.28};
    int   resultInt   = 0;
    float resultFloat = 0;

    stack stackOfInts;
    stack stackOfFloats;

    pid_t pid;
    pid = fork();

    if(pid == 0) {
        printf("creating stack of integers..\n");
        StackNew(&stackOfInts, sizeof(int));

        printf("push: %i\n", balls[0]);
        StackPush(&stackOfInts, &balls[0]);
        printf("push: %i\n", balls[1]);
        StackPush(&stackOfInts, &balls[1]);
        printf("push: %i\n", balls[2]);
        StackPush(&stackOfInts, &balls[2]);

        StackPop(&stackOfInts, &resultInt);
        printf("pop: %i\n", resultInt);
        StackPop(&stackOfInts, &resultInt);
        printf("pop: %i\n", resultInt);
        StackPop(&stackOfInts, &resultInt);
        printf("pop: %i\n", resultInt);

        StackDispose(&stackOfInts);
        printf("removing stack of integers..\n");
    }
    else {
        printf("creating stack of floats..\n");
        StackNew(&stackOfFloats, sizeof(float));

        printf("push: %f\n", rates[0]);
        StackPush(&stackOfFloats, &rates[0]);
        printf("push: %f\n", rates[1]);
        StackPush(&stackOfFloats, &rates[1]);
        printf("push: %f\n", rates[2]);
        StackPush(&stackOfFloats, &rates[2]);

        StackPop(&stackOfFloats, &resultFloat);
        printf("pop: %f\n", resultFloat);
        StackPop(&stackOfFloats, &resultFloat);
        printf("pop: %f\n", resultFloat);
        StackPop(&stackOfFloats, &resultFloat);
        printf("pop: %f\n", resultFloat);

        StackDispose(&stackOfFloats);
        printf("removing stack of floats..\n");
    }
    return 0;
}
